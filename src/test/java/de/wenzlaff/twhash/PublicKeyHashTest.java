package de.wenzlaff.twhash;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PublicKeyHashTest {

	@Test
	void testNull() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			PublicKeyHash.publicKeyHexToPublicKeyHash(null);
		});
	}

	@Test
	void testBlank() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			PublicKeyHash.publicKeyHexToPublicKeyHash("");
		});
	}

	@Test
	void testLength() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			PublicKeyHash.publicKeyHexToPublicKeyHash("dfafadfa");
		});
	}

	@Test
	void testOK() {
		assertEquals("35c2fccec44bb363c62442c2a7f1fc9600893c30",
				PublicKeyHash.publicKeyHexToPublicKeyHash("0326d92f3db1cfc3b158f5c97425dba8961e95f30c40d1c0e69efc5ac89d8d6ba2"));
	}
}