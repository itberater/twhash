package de.wenzlaff.twhash;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author Thomas Wenzlaff
 *
 */
public class CheckTest {

	@Test
	void testMainNull() {
		Check.main(null);
	}

	@Test
	void testMainLeer() {
		String[] parameter = {};
		Check.main(parameter);
	}

	@Test
	void testMainLeerStringSpace() {
		String[] parameter = { "" };
		Check.main(parameter);
	}

	@Test
	void testMainLeerSpace() {
		String[] parameter = { " " };
		Check.main(parameter);
	}

	@Test
	void testMainHashBlank() {
		String eingabeParameter = " ";
		assertEquals("36a9e7f1c95b82ffb99743e0c5c4ce95d83c9a430aac59f84ef3cbfab6145068", Check.getSHA256(eingabeParameter));
	}

	@Test
	void testMainHashLeer() {
		String eingabeParameter = "";
		assertEquals("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855", Check.getSHA256(eingabeParameter));
	}

	@Test
	void testMainHashKleinerName() {
		String eingabeParameter = "Thomas Wenzlaff";
		assertEquals("e635356aa96b74e29534d93ca2415a09cf4608df57d071e289fe008e22bf6e10", Check.getSHA256(eingabeParameter));
	}

	@Test
	void testMainHashVergleichKleinerName() {
		assertTrue(Check.checkSHA256("e635356aa96b74e29534d93ca2415a09cf4608df57d071e289fe008e22bf6e10", "Thomas Wenzlaff"));
	}

	@Test
	void testMainHashVergleichParameterVertauscht() {
		assertFalse(Check.checkSHA256("Thomas Wenzlaff", "e635356aa96b74e29534d93ca2415a09cf4608df57d071e289fe008e22bf6e10"));
	}
}
