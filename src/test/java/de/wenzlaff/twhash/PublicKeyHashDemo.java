package de.wenzlaff.twhash;

import io.nayuki.bitcoin.crypto.Ripemd160;
import io.nayuki.bitcoin.crypto.Sha256;
import io.nayuki.bitcoin.crypto.Sha256Hash;

/**
 * Bitcoin - Hashen des public Key zum PKH (public Key Hash)
 * 
 * Vorteil: Kürzer (von 33 auf 20 Byte, gleich 13 Byte gespart) und
 * verschleierter den public Key.
 * 
 * @author Thomas Wenzlaff
 */
public final class PublicKeyHashDemo {

	/**
	 * Public Key zu PKH
	 * 
	 * @param args optional der public Key (33 Byte) oder wenn keiner übergeben wird
	 *             ein Beispiel Key verwendet
	 */
	public static void main(String[] args) {

		System.out.println("Public Key (von Bitcoin 33 Byte) zum PKH (public Key Hash 20 Byte) transformieren:\n");

		String publicKey = null;
		if (args != null && args.length == 1) {
			publicKey = args[0];
		} else {
			// public Key 33 Byte oder 66 Hex Zeichen z.B.
			// 0326d92f3db1cfc3b158f5c97425dba8961e95f30c40d1c0e69efc5ac89d8d6ba2
			System.out.println("Verwende Beispiel public Key, da keiner übergeben wurde");
			publicKey = "0326d92f3db1cfc3b158f5c97425dba8961e95f30c40d1c0e69efc5ac89d8d6ba2";
		}

		System.out.println("Public Key (33 Byte):              " + publicKey);

		// 1. SHA 256
		Sha256Hash sha256Hash = Sha256.getHash(publicKey.getBytes());

		// 32 Byte oder 64 Hex Zeichen z.B.
		// af5c9a40eaacdbbb6ca346143ddaba7e22a099e0fd567f3318945ebc6297917c
		System.out.println("1. SHA-256 Hash erzeugen");
		System.out.println("SHA-256 Hash (32 Byte):            " + sha256Hash);

		// 2. RIPEMD-160
		byte[] pkh = Ripemd160.getHash(sha256Hash.toBytes());
		System.out.println("2. RIPEMD-160 Hash erzeugen");
		// 20 Byte oder 160 Bit PKH (Public Key Hash) oder 40 Hex Zeichen z.B.
		// 35c2fccec44bb363c62442c2a7f1fc9600893c30
		System.out.println("Public Key Hash (PKH, 20 Byte):    " + Transform.bytesToHex(pkh));
	}
}
