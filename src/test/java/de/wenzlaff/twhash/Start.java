package de.wenzlaff.twhash;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.common.hash.Hashing;

/**
 * Test von SHA-2 (von englisch secure hash algorithm, sicherer
 * Hash-Algorithmus) Hash.
 * 
 * @author Thomas Wenzlaff
 */
public class Start {

	private static String EINGABE = "Der Text von dem der Hash gebildet wird. Er soll auch mal ein paar Umlaute enthalten wie öü oder ÖÜ";

	public static void main(String[] args) throws Exception {

		System.out.println("Der SHA-256 Hash ist 32 Byte (2 HEX Zeichen * 32 = 64) lang, d.h. 64 Zeichen");

		// mit Java 8
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] hashJava = digest.digest(EINGABE.getBytes(StandardCharsets.UTF_8));

		System.out.println("Der SHA-256 Hash mit Java java.security.MessageDigest           : " + Transform.bytesToHex(hashJava));

		// mit Google Guava Lib
		String hashGuava = Hashing.sha256().hashString(EINGABE, StandardCharsets.UTF_8).toString();
		System.out.println("Der SHA-256 Hash mit Java com.google.guava                      : " + hashGuava);

		// mit Apache Lib
		String hashApache = DigestUtils.sha256Hex(EINGABE);
		System.out.println("Der SHA-256 Hash mit org.apache.commons.codec.digest.DigestUtils: " + hashApache);
	}
}