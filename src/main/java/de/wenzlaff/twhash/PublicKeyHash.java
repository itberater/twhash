package de.wenzlaff.twhash;

import io.nayuki.bitcoin.crypto.Ripemd160;
import io.nayuki.bitcoin.crypto.Sha256;

/**
 * Bitcoin - Hashen des public Key zum PKH (public Key Hash)
 * 
 * Vorteil: Kürzer (von 33 auf 20 Byte, gleich 13 Byte gespart) und
 * verschleierter den public Key.
 * 
 * @author Thomas Wenzlaff
 */
public final class PublicKeyHash {

	/**
	 * Transform eines public Bitcoin Key in Hex zu einem PKH.
	 * 
	 * @param publicHexKeyBitcoin der public Key im Hex Format
	 * @return der PKH (20 Byte)
	 */
	public static String publicKeyHexToPublicKeyHash(String publicHexKeyBitcoin) {

		String publicKey = null;
		if (publicHexKeyBitcoin != null && !publicHexKeyBitcoin.isEmpty() && publicHexKeyBitcoin.length() == 66) {
			publicKey = publicHexKeyBitcoin;
		} else {
			throw new IllegalArgumentException("Kein Bitcoin public Key übergeben, sondern: " + publicHexKeyBitcoin);
		}
		return Transform.bytesToHex(Ripemd160.getHash(Sha256.getHash(publicKey.getBytes()).toBytes()));
	}
}