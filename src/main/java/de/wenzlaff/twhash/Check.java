package de.wenzlaff.twhash;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.common.hash.Hashing;

/**
 * Berechne den Hash SHA-256 vom übergebenen Parameter.
 * 
 * Mit Java. Wenn der Algo. nicht vorhanden ist wird mit Apache und Guava
 * jeweils berechnet.
 * 
 * @author Thomas Wenzlaff
 */
public final class Check {

	private static final String FEHLER_MELDUNG = "Die Berechnung liefert unterschiedliche Hash-Werte. Das darf nicht vorkommen!";

	public static void main(String[] args) {

		if (args != null && args.length == 1) {
			String eingabe = args[0];
			getSHA256(eingabe);

		} else if (args != null && args.length == 2) {
			String hash = args[0];
			if (hash.length() != 64) {
				System.out.println("Es wurde kein 64 Zeichen langer HEX Hash SHA-256 als erster Parameter übergeben. Wurden die beiden Parameter vertauscht!");
				return;
			}
			String eingabe = args[1];

			boolean valid = checkSHA256(hash, eingabe);
			if (valid) {
				System.out.println("OK, Hash SHA-256 entspricht der Eingabe.");
			} else {
				System.out.println("ERROR, Hash SHA-256 entspricht nicht der Eingabe.");
			}

		} else {
			System.out.println(
					"Kein Parameter zum berechnen des Hash SHA-256 übergeben. Aufruf de.wenzlaff.twhash.Check [PARAMETER für den der Hash SHA-256 berechnet wird])");
			System.out.println("z.B. java -cp twhash-0.0.1-SNAPSHOT-jar-with-dependencies.jar de.wenzlaff.twhash.Check Cool");
			System.out.println("oder z.B.: java -jar twhash-0.0.1-SNAPSHOT-jar-with-dependencies.jar Cool");
		}
	}

	/**
	 * Checkt ob der Hash aus Parameter eins, mit der Eingabe übereinstimmt.
	 * 
	 * @param hash    Hash
	 * @param eingabe Eingabe
	 * @return wenn der Hash zu Eingabe passt dann true, sonst false
	 */
	public static boolean checkSHA256(String hash, String eingabe) {
		String berechneterHash = getSHA256(eingabe);
		return berechneterHash.equals(hash);
	}

	/**
	 * Liefert dern SHA-256 Hex Hash von drei Anbietern.
	 * 
	 * @param eingabe der String für den der Hash berechnet wird.
	 * @return der 64 Zeichen große SHA-256 Hash (256 Bits).
	 */
	public static String getSHA256(String eingabe) {

		System.out.println("Ermittle den SHA-256 Hash von: " + eingabe);

		// 1. mit Apache
		String hashApache = DigestUtils.sha256Hex(eingabe);

		// 2. mit Java
		MessageDigest digest = null;
		try {
			digest = MessageDigest.getInstance("SHA-256");
			byte[] hashJava = digest.digest(eingabe.getBytes(StandardCharsets.UTF_8));

			if (!hashApache.equals(Transform.bytesToHex(hashJava))) {
				System.err.println("Unterschiedliche Hash bei Vergleich Apache und Java! Bei dieser Eingabe: " + eingabe);
				System.err.println(FEHLER_MELDUNG);
			}
		} catch (NoSuchAlgorithmException e) {
			System.err.println("Kein Java SHA-256 Java Algorthmus vorhanden.");
		}

		// 3. mit Guava
		String hashGuava = Hashing.sha256().hashString(eingabe, StandardCharsets.UTF_8).toString();
		System.out.println("Der berechnete SHA-256 Hash  : " + hashGuava);

		if (!hashApache.equals(hashGuava)) {
			System.err.println("Unterschiedliche Hash bei Vergleich Apache und Guava! Bei dieser Eingabe: " + eingabe);
			System.err.println(FEHLER_MELDUNG);
		}

		return hashApache;
	}
}