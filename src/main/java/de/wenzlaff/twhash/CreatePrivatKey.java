package de.wenzlaff.twhash;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

/**
 * Privaten Bitcoin Key erzeugen.
 * 
 * @author Thomas Wenzlaff
 */
public class CreatePrivatKey {

	public static void main(String[] args) throws NoSuchAlgorithmException {

		byte[] privaterKey = new byte[PrivateKey.PRIVATE_KEY_GROESSE]; // immer geheim halten
		do {
			SecureRandom secRandom = SecureRandom.getInstanceStrong();
			System.out.println("Verwende Algorithmus (je nach System)                        : " + secRandom.getAlgorithm());

			secRandom.nextBytes(privaterKey);
		} while (!PrivateKey.isBitcoinPrivateKeyGueltig(privaterKey));

		System.out.println("Privater Key in String präsenations Format                   : " + Arrays.toString(privaterKey));
		System.out.println("Privater Key (32 Byte bei 8 Bit = 256 Bits = 64 Hex) erzeugen: " + Transform.bytesToHex(privaterKey)); // 24e8621ad9cab81a1aa5793665b376a58cfcb15fab02b28fa76b281401b7440c
	}
}