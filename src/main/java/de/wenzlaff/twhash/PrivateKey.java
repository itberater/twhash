package de.wenzlaff.twhash;

import java.math.BigInteger;

/**
 * Checkt die Gültigkeit eines Bitcoin privat Keys.
 * 
 * @author Thomas Wenzlaff
 */
public class PrivateKey {

	/**
	 * Die Länge des public Key in Bitcoin. 32-Byte.
	 */
	public static final int PRIVATE_KEY_GROESSE = 32;
	/**
	 * Der max. mögliche private Key Wert. Von 0x1
	 * 
	 * bis
	 * 
	 * 0xFFFF FFFF FFFF FFFF FFFF FFFF FFFF FFFE BAAE DCE6 AF48 A03B BFD2 5E8C D036
	 * 4140;
	 * 
	 * Siehe https://en.bitcoin.it/wiki/Private_key und
	 * https://en.bitcoin.it/wiki/Secp256k1
	 */
	private static final String MAX_ECDSA_PRIVATE_KEY = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364140";

	/**
	 * Checkt ob der private Key gültig ist. D.h. ob er die maximal gültige Größe
	 * nicht übersteigt.
	 * 
	 * @param privaterKey der zu überprüfende Key
	 * @return wenn der Key gültig ist wird true geliefert, wenn er ungültig ist
	 *         false.
	 */
	public static boolean isBitcoinPrivateKeyGueltig(byte[] privaterKey) {

		if (privaterKey.length != 32) {
			throw new IllegalArgumentException("Der private Key ist nicht 32-Byte lang");
		}

		BigInteger maxBitcoinPrivateKeyWert = new BigInteger(MAX_ECDSA_PRIVATE_KEY, 16);
		BigInteger key = new BigInteger(Transform.bytesToHex(privaterKey), 16);

		System.out.println("Privater Key in Dezimal                                      : " + key);
		System.out.println("Maximal möglicher privater Key in Dezimal                    : " + maxBitcoinPrivateKeyWert);

		return key.compareTo(maxBitcoinPrivateKeyWert) < 0 ? true : false;
	}
}